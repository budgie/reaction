const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
const app = require('../server.js');

chai.use(chaiHttp);

describe("Reactions", () => {
    //Test get route
    describe("GET /type", () => {
          //Get all type reaction
          it('should get all type reactions', (done) => {
              chai.request(app)
                .get('/type')
                .end((err, res) => {
                  res.should.have.status(200);
                  done();
              });
          });

            //Get a reaction by id
          it("should get a single reaction by id", (done) => {
            const id = "2014566e-dc0e-465f-9332-e83657a1b4d8";
            chai.request(app)
            .get(`/reactions/${id}`)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
          });

          //Get a reaction by post
          it("should get reaction of a post", (done) => {
            const post = "2713aaaf-29fe-4568-8830-cf92a19f8db5";
            chai.request(app).
            get(`/reactions/posts/${post}`).
            end((err, res) => {
                res.should.have.status(200);
                done();
            });
          });

          //Get count reaction of a type 
          it("should get count reaction of a type", (done) => {
            const post = "2713aaaf-29fe-4568-8830-cf92a19f8db5";
            const type = "2713aaaf-29fe-4568-8830-cf92a19f8da5";
            chai.request(app).
            get(`/post/${post}/type/${type}`).
            end((err, res) => {
                res.should.have.status(200);
                done();
            });
          });
        });

/*
        describe('/DELETE/:id reaction', () => {
          //Delete a reaction
          it('it should DELETE a reaction given the id', (done) => {
              const reaction = {
                idreaction : '2014566e-dc0e-465f-9332-e83657a1b4d8',
                idpost: '2713aaaf-29fe-4568-8830-cf92a19f8db5',
                idutilisateur: '2713aaaf-dc0e-4568-8830-e83657a1b4d3',
                idtypereaction: 'ed0784fe-f6f5-11e9-90c2-024264400007'
              } 
                  
              chai.request(app)
              .delete('/reactions/' + reaction.idreaction)
              .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('idReaction').eql(`${reaction.idreaction}`);
                  done();
              });
          });
      });
      */
})