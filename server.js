const express = require('express')
const bodyParser = require('body-parser')
require('pg').defaults.ssl = true
const { Pool } = require('pg')
const fs = require('fs')
const cors = require("cors");

const { 
    getReaction, 
    getReactionByPostByUser, 
    createReaction, 
    updateReaction, 
    deleteReaction, 
    getPostReaction, 
    getTypeReaction ,
    getCountReactionByPostByTypeReaction
} = require('./reaction')

require('dotenv').config()

const pool = new Pool()

const app = express()

module.exports = app.listen(process.env.PORT, process.env.IP, () => {
    console.log('Listening on ' + process.env.IP + ':' + process.env.PORT)
});

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cors());

// Add a reaction, modify if it already exists
// {idPost: int, idUser: int, idTypeReaction: int}
app.post('/reactions', function (req, res) {
    const reaction = {
        idPost : req.body.idPost,
        idUser : req.body.idUser,
        idTypeReaction : req.body.idTypeReaction
    }

    getReactionByPostByUser(reaction.idPost, reaction.idUser)
        .then((result) => {
            if (result !== undefined) {
                
                if (result.idtypereaction == reaction.idTypeReaction) {
                    
                    deleteReaction(result.idreaction)
                        .then(result => res.status(200).json({idReaction: result}))
                        .catch(err => res.status(500).json(err))
                } else {
                    
                    updateReaction(result.idreaction, reaction)
                        .then(result => res.status(200).json({idReaction: result}))
                        .catch(err => res.status(500).json(err))
                }
            } else {
                
                console.log('createReaction')
                createReaction(reaction)
                    .then(result => res.status(200).json({idReaction: result}))
                    .catch(err => res.status(502).json(err))
            }
        })
        .catch(err => {console.log(err);res.status(501).json(err)})
})

// Get a reaction
app.get('/reactions/:idReaction', function (req, res) {
    const idReaction = req.params.idReaction

    getReaction(idReaction)
        .then(resultat => {res.status(200).json(resultat)})
        .catch(err => res.status(500).send(err))
})

// Get a reaction of a post
app.get('/reactions/posts/:idPost', function (req, res) {
    const idPost = req.params.idPost

    getPostReaction(idPost)
        .then(resultat => {res.status(200).json(resultat)})
        .catch(err => res.status(500).send(err))
})

// Delete a reaction
app.delete('/reactions/:idReaction', function (req, res) {
    const idReaction = req.params.idReaction

    deleteReaction(idReaction)
        .then(res => res.status(200).json({idReaction: res}))
        .catch(err => res.status(500).send(err))
})

app.get('/type', function (req, res) {
    console.log('test')
    getTypeReaction()
        .then(resultat => {res.status(200).json({reactions: resultat})})
        .catch(err => {console.log(err);res.status(500).send(err)})
})

app.get('/post/:idPost/type/:idTypeReaction', (req, res) => {    
    const idPost = req.params.idPost
    const idTypeReaction = req.params.idTypeReaction

    getCountReactionByPostByTypeReaction(idPost, idTypeReaction)
        .then(resultat => {res.status(200).json(resultat)})
        .catch(err => {console.log(err);res.status(500).send(err)})
})

