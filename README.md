# Réaction

Le micro-service Réaction gère les réactions sous les publications.


### Pré-requis

Dans un premier temps, il faut installer  [npm](https://www.npmjs.com/get-npm) et [nodejs](https://nodejs.org/en/download/).

### Installation

Une fois le projet récupéré, installer toutes les dépendances.

```
npm install
```

Une fois les dépendances installées, mettre dans .env les lignes suivantes.
```
IP=localhost
PORT=<Port>
PGUSER=<User>
PGHOST=<Host>
PGPASSWORD=<Password>
PGDATABASE=<DBName>
PGPORT=<DBPort>
PGSSLMODE=require
```

### Lancer

Une fois que tout est installé, faire la commande suivante pour lancer le service

```
node server.js
```


