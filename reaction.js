const { Pool } = require('pg')
require('dotenv').config()

const pool = new Pool()

// Create a reaction
// returns the id of the new reaction
function createReaction(reaction) {
    
    return new Promise (async function (resolve, reject) {
        const client = await pool.connect()
        
        client.query(
            `INSERT INTO reactions (idReaction ,idPost, idUtilisateur, idTypeReaction)
            VALUES (uuid_generate_v1(), $1, $2, (
                SELECT typeReactions.idTypeReaction
                FROM typeReactions
                WHERE typeReactions.idTypeReaction = $3
            ))
            RETURNING reactions.idReaction`, 
            [reaction.idPost, reaction.idUser, reaction.idTypeReaction], 
            (err, res) => {
                client.release()

                if (err) {
                    console.log(err)
                    reject(err)
                } else {
                    
                    resolve(res.rows[0].idreaction)
                }
            }
        )
    })
}

// Create a reaction
function updateReaction(idReaction, reaction) {
    return new Promise (async function (resolve, reject) {
        const client = await pool.connect()
        
        client.query({
            text: `UPDATE reactions
            SET idpost = $1, 
                idutilisateur = $2, 
                idtypereaction = $3
            WHERE idreaction = $4`,
           values:  [reaction.idPost, reaction.idUser, reaction.idTypeReaction, idReaction]
        },
            (err, res) => {
                client.release()
                if (err) {
                    
                    reject(err)
                } else {
                    resolve(idReaction)
                }
            }
        )
    })
}

function getReactionByPostByUser(idPost, idUser) {
    return new Promise(async function (resolve, reject) {
        const client = await pool.connect()
        client.query(
            {
                text: `
                    SELECT reactions.idReaction, 
                        reactions.idPost, 
                        reactions.idUtilisateur, 
                        reactions.idTypeReaction,
                        typeReactions.nom 
                    FROM reactions 
                        JOIN typeReactions ON reactions.idTypeReaction = typeReactions.idTypeReaction
                    WHERE reactions.idPost = $1 
                        AND reactions.idUtilisateur = $2`,
                values:  [idPost, idUser]
            },
            (err, res) => {   
                client.release()             
                if (err) {
                    reject(err)
                } else {
                    resolve(res.rows[0])
                }
            }
        )
    })
}

function deleteReaction(idReaction) {
    return new Promise(async function (resolve, reject) {
        const client = await pool.connect()
        client.query(
            {
                text: `DELETE FROM reactions 
                    WHERE idReaction = $1`,
                values: [idReaction]
            },
            (err, res) => {     
                client.release()           
                if (err) {
                    reject(err)
                } else {
                    resolve(idReaction)
                }
            }
        )
    })
}

function getReaction(idReaction) {
    return new Promise(async function (resolve, reject) {
        const client = await pool.connect()
        client.query({
            text: `SELECT *
            FROM reactions 
            WHERE idReaction = $1`,
            values: [idReaction]
        })
        .then(res => {
            client.release()
            resolve(
                res.rows === undefined ? null : res.rows.length > 1 || res.rows.length < 1 ? null : res.rows[0]
            )
        })
        .catch(err => {
            client.release()
            reject(err)
        })
    })
}

function getPostReaction(idPost) {
    return new Promise(async function (resolve, reject) {
        const client = await pool.connect()
                
        client.query(
            {
                text: `
                    SELECT typeReactions.valeur,
                        typeReactions.nom,
                        count(reactions.idReaction) AS countIdReaction
                    FROM reactions
                        JOIN typeReactions ON reactions.idTypeReaction = typeReactions.idTypeReaction
                    WHERE reactions.idPost = $1
                    GROUP BY typeReactions.valeur, typeReactions.nom`,
                values:  [idPost]
            },
            (err, res) => {                
                client.release()
                if (err) {
                    reject(err)
                } else {
                    resolve(res.rows)
                }
            }
        )
    })
}

function getTypeReaction() {
    return new Promise(async function (resolve, reject) {
        const client = await pool.connect()
                
        client.query(
            {
                text: `
                    SELECT *
                    FROM typeReactions`
            },
            (err, res) => { 
                client.release()               
                if (err) {
                    console.log(err)
                    reject(err)
                } else {
                    resolve(res.rows)
                }
            }
        )
    })
}

function getCountReactionByPostByTypeReaction(idPost, idTypeReaction) {
    
    return new Promise(async function (resolve, reject) {
        const client = await pool.connect()
        client.query(
            {
                text: `
                    SELECT count(reactions.idReaction) AS countTypeReaction
                    FROM reactions
                    WHERE reactions.idPost = $1
                        AND reactions.idTypeReaction = $2`,
                values: [idPost, idTypeReaction]
            },
            (err, res) => {     
                client.release()
                          
                if (err) {
                    reject(err)
                } else {
                    resolve(res.rows[0].counttypereaction)
                }
            }
        )
    })
}

module.exports = { 
    createReaction, 
    updateReaction, 
    getReaction,
    getReactionByPostByUser, 
    deleteReaction, 
    getPostReaction, 
    getTypeReaction,
    getCountReactionByPostByTypeReaction
}